package exercise;

// BEGIN
public class Flat implements Home{
    private double area;
    private double balconyArea;
    private Integer floor;

    public Flat(double area, double balconyArea, Integer floor) {
        this.area = area;
        this.balconyArea = balconyArea;
        this.floor = floor;
    }

    @Override
    public double getArea() {
       return area + balconyArea;
    }

    @Override
    public int compareTo(Home anotherHome) {
        if (anotherHome.getArea() < this.getArea()) {
            return 1;
        } else if (anotherHome.getArea() > this.getArea()) {
            return -1;
        }
        return 0;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Квартира площадью ").append(getArea())
                .append(" метров на ").append(floor).append(" этаже");
        return sb.toString();
    }
}
// END
