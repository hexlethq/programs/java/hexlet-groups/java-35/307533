package exercise;

// BEGIN
public class Cottage implements Home{
    private double area;
    private Integer floorCount;

    public Cottage(double area, Integer floorCount) {
        this.area = area;
        this.floorCount = floorCount;
    }

    @Override
    public double getArea() {
        return area;
    }

    @Override
    public int compareTo(Home anotherHome) {
        if (anotherHome.getArea() < this.getArea()) {
            return 1;
        } else if (anotherHome.getArea() > this.getArea()) {
            return -1;
        }
        return 0;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(floorCount).append(" этажный коттедж площадью ").append(area).append(" метров");
        return sb.toString();
    }
}
// END
