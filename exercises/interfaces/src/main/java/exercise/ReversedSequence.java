package exercise;

// BEGIN
public class ReversedSequence implements CharSequence{
    private String sequence;

    public ReversedSequence(String sequence) {
        this.sequence = sequence;
    }

    @Override
    public int length() {
        return sequence.length();
    }

    @Override
    public char charAt(int i) {
        return sequence.charAt(i);
    }

    @Override
    public CharSequence subSequence(int i, int i1) {
        return sequence.subSequence(i, i1);
    }

    @Override
    public String toString() {
        return new StringBuilder(sequence).reverse().toString();
    }
}
// END
