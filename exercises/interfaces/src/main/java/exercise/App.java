package exercise;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

// BEGIN
public class App {
    public static List<String> buildAppartmentsList(List<Home> houses, int counter) {
        return houses.stream()
                .sorted(Comparator.comparingDouble(Home::getArea))
                .limit(counter)
                .map(Home::toString)
                .collect(Collectors.toList());
    }
}
// END
