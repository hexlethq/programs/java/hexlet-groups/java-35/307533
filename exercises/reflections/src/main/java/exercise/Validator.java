package exercise;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// BEGIN
public class Validator {
    public static List<String> validate(Address address) {
        List<String> newList = new ArrayList<>();
        for (Field field : address.getClass().getDeclaredFields()) {
            NotNull nullField = field.getAnnotation(NotNull.class);
            field.setAccessible(true);
            try {
                if (nullField != null && field.get(address) == null) {
                    newList.add(field.getName());
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        System.out.println(advancedValidate(address));
        return newList;
    }

    public static Map<String, List<String>> advancedValidate(Address address) {
        Map<String, List<String>> sortedValidate = new HashMap<>();
        for (Field field : address.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            NotNull nullField = field.getAnnotation(NotNull.class);
            MinLength minField = field.getAnnotation(MinLength.class);
            try {
                if (nullField != null && field.get(address) == null) {
                    sortedValidate.put(field.getName(), List.of("can not be null"));
                    continue;
                }
                if (minField != null && field.get(address).toString().length() < minField.minLength()) {
//                    field.get(address).toString().length();
                    sortedValidate.put(field.getName(), List.of("length less than 4"));
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return sortedValidate;
    }
}
// END
