package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(int i, int i1, int i2) {
        double iRadians = i2 * Math.PI / 180;
        double iSquare = ((i * i1) / 2) * Math.sin(iRadians);
        return iSquare;
    }

    public static void main(String [] args) {
        System.out.println(getSquare(4, 5 , 45));
    }
    // END
}
