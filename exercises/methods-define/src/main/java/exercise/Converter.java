package exercise;

class Converter {
    // BEGIN
    public static int convert(int x, String y) {
        int size = 0;
        if (y == "b") {
            size = x * 1024;
        } else if (y == "Kb") {
            size = x / 1024;
        }
        return size;
    }

    public static void main() {
        System.out.println("10 Kb = " + convert(10, "b") + " b");
    }
    // END
}
