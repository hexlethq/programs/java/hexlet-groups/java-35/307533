package exercise.connections;

import exercise.TcpConnection;

import java.util.ArrayList;
import java.util.List;

// BEGIN
public class Connected implements Connection{
    private TcpConnection connection;
    private List<String> buffer = new ArrayList<>();

    public Connected(TcpConnection connection) {
        this.connection = connection;
    }

    @Override
    public String getCurrentState() {
        return "connected";
    }

    @Override
    public void connect() {
        System.out.println("Error! Connection already connected");
    }

    @Override
    public void disconnect() {
        TcpConnection conn = this.connection;
        conn.setState(new Disconnected(conn));
    }

    @Override
    public void write(String date) {
        this.buffer.add(date);
    }

}
// END
