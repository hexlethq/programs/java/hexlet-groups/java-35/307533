package exercise;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// BEGIN
public class App {
    public static Map<String, Integer> getWordCount(String words) {
        Map<String, Integer> wordCount = new HashMap<>();
        if (words.isEmpty()) {
            return wordCount;
        }
        String[] splitWords = words.split("\\s");
        for (String word: splitWords) {
            Integer counter = wordCount.get(word);
            if (counter == null) {
                counter = 0;
            }
            wordCount.put(word, counter + 1);
        }
        return wordCount;
    }

    public static String toString(Map<String, Integer> wordCount) {
        if (wordCount.isEmpty()) return "{}";
        String toString = "{\n";
        for ( String word : wordCount.keySet() ) {
            toString = toString + "  " + word + ": " + wordCount.get(word) + "\n";
        }
        toString = toString + "}";
        return toString;
    }
}
//END
