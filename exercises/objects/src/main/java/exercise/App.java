package exercise;

import javax.xml.crypto.Data;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

class App {
    // BEGIN
    public static String buildList(String[] html) {
        StringBuilder newHTML = new StringBuilder();
        if (html.length == 0) {
            return "";
        }
        newHTML.append("<ul>\n");
        for (int i = 0; i < html.length; i++) {
            newHTML.append("  <li>" + html[i] + "</li>\n");
        }
        newHTML.append("</ul>");
        String str = newHTML.toString();

        return str;
    }

    public static String getUsersByYear(String[][] users, int year) {
        String[] yearOfUsers = new String[users.length];
        int counter = 0;
        int yearCounter = 0;
        if (users.length == 0) {
            System.out.println("Массив пустой");
        }
        for (int i = 0; i < users.length; i++) {
            LocalDate date = LocalDate.parse(users[i][1]);
            int toYear = date.getYear();

            if (toYear == year) {
                yearOfUsers[counter] = users[i][0];
                yearCounter ++;
                counter++;
            }
        }
        String[] yearOfUsers2 = Arrays.copyOf(yearOfUsers, yearCounter);
        return App.buildList(yearOfUsers2);
    }
        // END

        // Это дополнительная задача, которая выполняется по желанию.
        public static String getYoungestUser (String[][] users, String date) throws Exception {
            // BEGIN
           LocalDate beforeDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd MMM yyyy", Locale.ENGLISH));
           LocalDate youngestDate = LocalDate.MIN;
           String youngestUser ="";
           for(int i = 0; i< users.length; i++){
               LocalDate currentDate = LocalDate.parse(users[i][1]);
               if (currentDate.isBefore(beforeDate) && currentDate.isAfter(youngestDate)){
                   youngestDate = currentDate;
                   youngestUser = users[i][0];
               }
           }
            return youngestUser;
            // END
        }
    }
