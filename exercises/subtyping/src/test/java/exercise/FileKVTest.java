package exercise;

import java.util.HashMap;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
// BEGIN
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
// END


class FileKVTest {

    private static Path filepath = Paths.get("src/test/resources/file").toAbsolutePath().normalize();

    @BeforeEach
    public void beforeEach() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String content = mapper.writeValueAsString(new HashMap<String, String>());
        Files.writeString(filepath, content, StandardOpenOption.CREATE);
    }

    // BEGIN
    @Test
    void fileKVTest() {
        KeyValueStorage storage = new FileKV(filepath.toString(), Map.of("key", "value"));

        assertThat(storage.get("key", "")).isEqualTo("value");

        storage.set("key2", "value1");
        storage.set("key3", "default");

        assertThat(storage.get("key2", "")).isEqualTo("value1");
        assertThat(storage.get("key3", "")).isEqualTo("default");

        storage.unset("key2");

        assertThat(storage.get("key2", "default")).isEqualTo("default");

        assertThat(storage.toMap()).isEqualTo(Map.of("key", "value", "key3", "default"));
    }

    @Test
    void mustBeImmutableTest() {
        Map<String, String> initial = new HashMap<>();
        initial.put("key", "10");

        Map<String, String> clonedInitial = new HashMap<>();
        clonedInitial.putAll(initial);

        KeyValueStorage storage = new FileKV(filepath.toString(), initial);

        initial.put("key2", "value2");
        Assertions.assertThat(storage.toMap()).isEqualTo(clonedInitial);

        Map<String, String> map = storage.toMap();
        map.put("key2", "value2");
        Assertions.assertThat(storage.toMap()).isEqualTo(clonedInitial);
    }
    // END
}
