package exercise;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

// BEGIN
public class App {
    public static void swapKeyValue(KeyValueStorage storage) {
        Map<String, String> map = storage.toMap();
        map.keySet().forEach(s -> {
            storage.set(map.get(s), s);
            if (storage.toMap().containsKey(s)) {
                storage.unset(s);
            }
        });
    }
}
// END
