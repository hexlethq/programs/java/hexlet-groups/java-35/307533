package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class InMemoryKV implements KeyValueStorage{
    private Map<String, String> startValue;

    public InMemoryKV(Map<String, String> startValue) {
        this.startValue = new HashMap<>(startValue);
    }

    @Override
    public void set(String key, String value) {
        startValue.put(key, value);
    }

    @Override
    public void unset(String key) {
        startValue.remove(key);
    }

    @Override
    public String get(String key, String defaultValue) {
        return startValue.getOrDefault(key, defaultValue);
    }

    @Override
    public Map<String, String> toMap() {
        return new HashMap<>(this.startValue);
    }
}
// END
