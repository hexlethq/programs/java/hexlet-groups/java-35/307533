package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class FileKV implements KeyValueStorage{
    private final String path;
    private Map<String, String> startValue;

    public FileKV(String path, Map<String, String> startValue) {
        this.path = path;
        this.startValue = new HashMap<>(startValue);
        Utils.writeFile(path, Utils.serialize(startValue));
    }

    @Override
    public void set(String key, String value) {
        startValue = Utils.unserialize(Utils.readFile(path));
        startValue.put(key, value);
        Utils.writeFile(path, Utils.serialize(startValue));
    }

    @Override
    public void unset(String key) {
        startValue = Utils.unserialize(Utils.readFile(path));
        startValue.remove(key);
        Utils.writeFile(path, Utils.serialize(startValue));
    }

    @Override
    public String get(String key, String defaultValue) {
        startValue = Utils.unserialize(Utils.readFile(path));
        return startValue.getOrDefault(key, defaultValue);
    }

    @Override
    public Map<String, String> toMap() {
        return Utils.unserialize(Utils.readFile(path));
    }
}
// END
