package exercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static void main(String[] args) {
        String[][] image1 = {
                {"*", "*", "*", "*"},
                {"*", " ", " ", "*"},
                {"*", " ", " ", "*"},
                {"*", "*", "*", "*"},
        };

        System.out.println(Arrays.deepToString(enlargeArrayImage(image1)));
    }
    public static String[][] enlargeArrayImage(String[][] image) {
        return Arrays.stream(image)
                .map(App::duplicateValues)
                .flatMap(vertical -> Stream.of(vertical,vertical))
                .toArray((String[][]::new));
    }

    public static String[] duplicateValues(String[] items) {
        return Arrays.stream(items)
                .flatMap(item -> Arrays.stream(new String[]{item, item}))
                .toArray(String[]::new);
    }
}
// END
