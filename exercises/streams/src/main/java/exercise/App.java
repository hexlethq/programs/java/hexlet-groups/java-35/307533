package exercise;

import java.util.*;
import java.util.stream.Collectors;

// BEGIN
public class App {
    
    public static long getCountOfFreeEmails(List<String> emails) {
        long contain = 0;
        if (emails != null) {
            contain = emails.stream()
                    .filter(email -> email.contains("@gmail.com"))
                    .count();
            contain = contain + emails.stream()
                    .filter(email -> email.contains("@yandex.ru"))
                    .count();
            contain = contain + emails.stream()
                    .filter(email -> email.contains("@hotmail.com"))
                    .count();
        }
        return contain;
    }

//    public static long getCountOfFreeEmails(List<String> emails) {
//        Map<Boolean, List<String>> contains = new HashMap<>();
//        long contain = 0;
//        if (emails != null) {
//            contains = emails.stream()
//                    .filter(Objects::nonNull)
//                    .collect(Collectors.groupingBy(email -> email.contains("gmail") || email.contains("@yandex.ru") ||
//                            email.contains("@hotmail.com"), Collectors.toList()));
//            for (Map.Entry<Boolean, List<String>> entry : contains.entrySet()) {
//                if (entry.getKey()) {
//                    contain = entry.getValue().size();
//                }
//            }
//        }
//        return contain;
//    }

}
// END
