package exercise;

import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class PairedTag extends Tag {
    private String name;
    private String body;
    private Map<String, String> attributes;
    private List<Tag> children;

    public PairedTag(String name, Map<String, String> attributes, String body, List<Tag> children) {
        super(name, attributes);
        this.name = name;
        this.body = body;
        this.attributes = attributes;
        this.children = children;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder("<" + name);
        for (String key : attributes.keySet()) {
            builder.append(" ").append(key).append("=").append("\"").append(attributes.get(key))
                    .append("\"");
        }

        if (children.isEmpty()) {
            return builder.append(">").append(body).append("</").append(name).append(">").toString();
        } else {
            builder.append(">");
            for (Tag single : children) {
                builder.append(single.toString());
            }
            return builder + "</" + name + ">";
        }
    }
}
// END
