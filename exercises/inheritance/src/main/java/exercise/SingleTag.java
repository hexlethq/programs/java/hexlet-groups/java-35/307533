package exercise;

import java.util.Map;

// BEGIN
public class SingleTag extends Tag {
    private String name;
    private Map<String, String> attributes;

    public SingleTag(String name, Map<String, String> attributes) {
        super(name, attributes);
        this.name = name;
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("<" + name);
        for (String key : attributes.keySet()) {
            builder.append(" ").append(key).append("=")
                    .append("\"").append(attributes.get(key)).append("\"");
        }
        return builder.toString().trim() + ">";
    }
}
// END
