package exercise;

import com.sun.jdi.Value;

import java.util.*;

// BEGIN
public class App {
    public static void main(String[] args) {

        Map<String, Object> data1 = new HashMap<>(
                Map.of("one", "eon", "two", "two", "four", true, "abs", 'h')
        );
        Map<String, Object> data2 = new HashMap<>(
                Map.of("two", "own", "zero", 4, "four", true)
        );
        System.out.println(genDiff(data1, data2));
    }

    public static LinkedHashMap<String, String> genDiff(Map<String, Object> first, Map<String, Object> second) {
        LinkedHashMap<String, String> expected = new LinkedHashMap<>();
        for (String key : first.keySet()) {
            if (second.containsKey(key)) {
                if (first.get(key).equals(second.get(key))) {
                    expected.put(key, "unchanged");
                } else {
                    expected.put(key, "changed");
                }
            } else {
                expected.put(key, "deleted");
            }
        }
        for (String key: second.keySet()) {
            if (!first.containsKey(key)) {
                expected.put(key, "added");
            }
        }
        return expected;

    }
}
//END
