package exercise.controllers;

import io.javalin.http.Context;
import io.javalin.apibuilder.CrudHandler;
import io.ebean.DB;
import java.util.List;

import exercise.domain.User;
import exercise.domain.query.QUser;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.lang3.StringUtils;

public class UserController implements CrudHandler {

    public void getAll(Context ctx) {
        // BEGIN
        List<User> usersList = new QUser()
                .orderBy()
                .id.asc()
                .findList();

        String users = DB.json().toJson(usersList);
        ctx.json(users);
        // END
    };

    public void getOne(Context ctx, String id) {

        // BEGIN
        User user = new QUser()
                .orderBy()
                .id.equalTo(Integer.parseInt(id))
                .findOne();

        String userJSON = DB.json().toJson(user);
        ctx.json(userJSON);
        // END
    };

    public void create(Context ctx) {

        // BEGIN
        String body = ctx.body();
        ctx.bodyValidator(User.class)
                .check(it -> !it.getFirstName().isEmpty(), "Имя не должно быть пустым")
                .check(it -> !it.getLastName().isEmpty(), "Имя не должно быть пустым")
                .check(it -> EmailValidator.getInstance().isValid(it.getEmail()), "Не валидный емаил")
                .check(it -> it.getPassword().length() >= 4, "Password length must be more than 4 symbols")
                .check(it -> StringUtils.isNumeric(it.getPassword()), "Количество пользователей должно содержать только цифры")
                .get();

        User user = DB.json().toBean(User.class, body);
        user.save();
        // END
    };

    public void update(Context ctx, String id) {
        // BEGIN
        String body = ctx.body();
        User user = DB.json().toBean(User.class, body);
        user.setId(id);
        user.update();
        // END
    };

    public void delete(Context ctx, String id) {
        // BEGIN
        User user = new QUser()
                .orderBy()
                .id.equalTo(Integer.parseInt(id))
                .findOne();
        user.delete();
        // END
    };
}
