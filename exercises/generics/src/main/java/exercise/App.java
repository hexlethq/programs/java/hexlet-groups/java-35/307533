package exercise;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Map.Entry;

// BEGIN
public class App {
    public static List<Map<String, String>> findWhere(List<Map<String, String>> book, Map<String, String> where) {
        List<Map<String, String>> finder = new ArrayList<>();
        for (Map<String, String> listNumber : book) {
            int counter = 0;
            for (String mapNumber : where.keySet()) {
                if (listNumber.get(mapNumber).equals(where.get(mapNumber))) {
                    counter++;
                    if (counter == where.size()) {
                        finder.add(listNumber);
                    }
                }
            }
        }
        return finder;
    }
}
//END
