package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.Map;

import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.ArrayUtils;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUser(request, response, id);
    }

    private List getUsers() throws JsonProcessingException, IOException {
        // BEGIN
        String data = Files.readString(Path.of("./src/main/resources/users.json").toAbsolutePath().normalize());
        return new ObjectMapper().<List<Map<String, String>>>readValue(data, new TypeReference<>() {
        });
        // END
    }

    private void showUsers(HttpServletRequest request,
                           HttpServletResponse response)
            throws IOException {

        // BEGIN
        List<Map<String, String>> users = getUsers();

        response.setContentType("text/html;charset=UTF-8");

        StringBuilder html = new StringBuilder();
        html.append("""
                <!DOCTYPE html>
                <html lang="ru">
                    <head>
                        <meta charset="UTF-8">
                        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" 
                        rel="stylesheet"
                        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" 
                        crossorigin="anonymous">
                        <title>Users Servlet</title>                            
                    </head>
                    <body>
                    <table class="table">
                              <thead>
                                <tr>
                                  <th scope="col">id</th>
                                  <th scope="col">Name</th>
                                </tr>
                              </thead>
                              <tbody>""");
        for (Map<String, String> user : users) {
            html.append("<tr>\n" + "      <th scope=\"row\">").append(user.get("id")).append("</th>\n")
                    .append("      <td>").append("<a href=\"/users/").append(user.get("id")).append("\">")
                    .append(user.get("firstName")).append(" ").append(user.get("lastName")).append("</a>")
                    .append("</td>\n").append("    </tr>");
        }
        html.append("</tbody>")
                .append("</table>")
                .append("</body>")
                .append("</html>");

        PrintWriter writer = response.getWriter();
        writer.println(html);
        // END
    }

    private void showUser(HttpServletRequest request,
                          HttpServletResponse response,
                          String id)
            throws IOException {

        // BEGIN
        List<Map<String, String>> users = getUsers();

        response.setContentType("text/html;charset=UTF-8");

        StringBuilder html = new StringBuilder();
        html.append("""
                <!DOCTYPE html>
                <html lang="ru">
                    <head>
                        <meta charset="UTF-8">
                        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" 
                        rel="stylesheet"
                        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" 
                        crossorigin="anonymous">
                        <title>User personal data</title>                            
                    </head>
                    <body>
                    <table class="table">
                              <thead>
                                <tr>
                                  <th scope="col">First name</th>
                                  <th scope="col">Last name</th>
                                  <th scope="col">id</th>
                                  <th scope="col">email</th>
                                </tr>
                              </thead>
                              <tbody>""");
        if (checkUser(users, id)) {
            for (Map<String, String> user : users) {
                if (user.get("id").equals(id)) {
                    html.append("      <td>").append(user.get("firstName")).append("</th>\n")
                            .append("      <td>").append(user.get("lastName")).append("</td>\n")
                            .append("      <td>").append(user.get("id")).append("</td>\n")
                            .append("      <td>").append(user.get("email")).append("</td>\n")
                            .append("</td>\n").append("    </tr>");
                }
            }
            html.append("</tbody>")
                    .append("</table>")
                    .append("</body>")
                    .append("</html>");
            PrintWriter writer = response.getWriter();
            writer.println(html);
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
        // END
    }

    private static boolean checkUser(List<Map<String, String>> users, String id) {
        return users.stream()
                .anyMatch(user -> user.get("id").equals(id));
    }
}
