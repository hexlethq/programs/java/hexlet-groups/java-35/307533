package exercise;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

// BEGIN
public class App {
    public static boolean scrabble(String kit, String word) {
        if (kit.isEmpty()) {
            return false;
        }
        List<String> exam = new ArrayList<>();
        for (int i = 0; i < kit.length(); i++){
            exam.add(String.valueOf(kit.charAt(i)));
        }
        for (int i = 0; i < word.length(); i++){
            String character = String.valueOf(word.toLowerCase().charAt(i));
            if (exam.contains(character)) {
                exam.remove(character);
            } else {
                return false;
            }
        }
        return true;
    }
}
//END
