<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->
<%@ page import ="java.util.List"%>
<%@ page import ="java.util.Map"%>
<!DOCTYPE html>
  <html lang="ru">
    <head>
        <meta charset="UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous">
        <title>Users</title>
    </head>
    <body>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">First name</th>
                    <th scope="col">Last name</th>
                    <th scope="col">id</th>
                    <th scope="col">email</th>
                    <th scope="col">Delete user</th>
                </tr>
            </thead>
            <%
               Map<String, String> user = (Map<String, String>) request.getAttribute("user");
            %>
                <tr>
                    <td><%=user.get("firstName")%></td>
                    <td><%=user.get("lastName")%></td>
                    <td><%=user.get("id")%></td>
                    <td><%=user.get("email")%></td>
                    <td><a href=<%="/users/delete?id=" + user.get("id")%>>Delete</a></td>
                </tr>
            <tbody>
            </tbody>
        </table>
    </body>
  </html>
<!-- END -->
