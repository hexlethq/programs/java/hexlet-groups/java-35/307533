<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->
<%@ page import ="java.util.List"%>
<%@ page import ="java.util.Map"%>
<!DOCTYPE html>
  <html lang="ru">
    <head>
        <meta charset="UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous">
        <title>Users</title>
    </head>
    <body>
        <table class="table">
            <%
               Map<String, String> user = (Map<String, String>) request.getAttribute("user");
            %>
                <tr>
                    <form action="url" method="post">
                        <td>Delete user<%=" " + user.get("firstName") + " " + user.get("lastName")%>?</td>
                        <td><button type="submit" class="btn btn-danger">Yes, delete this user</button></td>
                    </form>
                </tr>
            <tbody>
            </tbody>
        </table>
    </body>
  </html>
<!-- END -->
