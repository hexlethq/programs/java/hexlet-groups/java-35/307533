<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->
<%@ page import ="java.util.List"%>
<%@ page import ="java.util.Map"%>
<!DOCTYPE html>
  <html lang="ru">
    <head>
        <meta charset="UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous">
        <title>Users</title>
    </head>
    <body>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Name</th>
                </tr>
            </thead>
            <%
               List<Map<String, String>> users = (List<Map<String, String>>) request.getAttribute("users");
            %>
                <%
                for (Map<String, String> user: users) { %>
                <tr>
                    <th scope="row"><%=user.get("id")%></th>
                    <td>
                        <a href=<%="/users/show?id=" + user.get("id")%>><%=user.get("firstName") + " " + user.get("lastName")%></a>
                    </td>
                </tr>
                <%}%>
            <tbody>
            </tbody>
        </table>
    </body>
  </html>
<!-- END -->
