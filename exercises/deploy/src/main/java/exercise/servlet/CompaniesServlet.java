package exercise.servlet;

import exercise.Data;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static exercise.Data.getCompanies;

@WebServlet("/company")
public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        // BEGIN
        PrintWriter writeCompanies = response.getWriter();
        if (checkQueryString(request)) {

           if (checkEmptyStream(request)) {
               getCompanies().stream()
                       .filter(company -> company.toString().contains(request.getParameter("search")))
                       .forEach(writeCompanies::println);
           } else {
               writeCompanies.println("Companies not found");
           }

        } else {
            getCompanies().stream()
                    .forEach(writeCompanies::println);
        }
        writeCompanies.close();
        // END
    }

    private boolean checkQueryString(HttpServletRequest request) {
        return request.getQueryString() != null && !request.getParameter("search").equals("");
    }

    private boolean checkEmptyStream(HttpServletRequest request) {
        return (boolean) getCompanies().stream()
                .filter(company -> company.toString().contains(request.getParameter("search")))
                .map(x -> Boolean.TRUE)
                .findAny()
                .orElse(Boolean.FALSE);
    }
}
