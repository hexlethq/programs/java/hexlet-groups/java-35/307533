package exercise;

import java.util.Arrays;
import java.util.stream.Collectors;

// BEGIN
public class App {
    public static void main(String[] args) {
    String config = "environment=\"X_FORWARDED_mail=tirion@google.com,X_FORWARDED_HOME=/home/tirion,language=en\"";
    System.out.println(getForwardedVariables(config));
    }
    public static String getForwardedVariables(String config) {
        return config.lines()
                .filter(x -> x.startsWith("environment=\""))
                .map(x -> x.replaceAll("environment=\"", ""))
                .map(x -> x.replaceAll("\\s*\"\\s*$", ""))
                .filter(x -> x.contains("X_FORWARDED_"))
                .map(x -> x.split(","))
                .flatMap(Arrays::stream)
                .filter(x -> x.startsWith("X_FORWARDED_"))
                .map(x -> x.replaceAll("X_FORWARDED_", ""))
                .collect(Collectors.joining(","));
    }
}
//END
