package exercise;

import java.lang.reflect.Array;
import java.util.Arrays;

class Point {
    // BEGIN
    public static int[] makePoint(int x, int y){
        int[] point = {x, y};
        return point;
    }
    public static int getX(int[] getx){
         int X = getx[0];
         return X;
    }
    public static int getY(int[] gety){
        int Y = gety[1];
        return Y;
    }
    public static String pointToString(int[] pointString){
        String str = "(" +  Point.getX(pointString) + ", " + Point.getY(pointString) + ")";
        return str;
    }
    public static int getQuadrant(int[] quadrant){
//        if(Point.getX(quadrant) == 0 || Point.getY(quadrant) == 0){
//            return 0;
//        }

        if(Point.getX(quadrant) > 0 && Point.getY(quadrant) > 0){
            return 1;
        }else if(Point.getX(quadrant) < 0 && Point.getY(quadrant) > 0){
            return 2;
        }else if(Point.getX(quadrant) < 0 && Point.getY(quadrant) < 0){
            return 3;
        }else if(Point.getX(quadrant) > 0 && Point.getY(quadrant) < 0){
            return 4;
        }else{
            return 0;
        }
    }

    //самостоятельная работа
    public static int[] getSymmetricalPointByX(int[] newPoint){
        int[] symmetricalPoint = new int[newPoint.length];
            symmetricalPoint[0] = Point.getX(newPoint);
            symmetricalPoint[1] = Point.getY(newPoint) * (-1);
            return symmetricalPoint;
    }

    public static double calculateDistance(int[] firstPoint, int[] secondPoint){
        double distance = Math.sqrt((Point.getX(secondPoint) - Point.getX(firstPoint))^2 + (Point.getY(secondPoint) - Point.getY(firstPoint))^2);
        return distance;
    }
    // END
}
