package exercise;

import org.junit.jupiter.api.Test;

import static com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOut;
import static org.assertj.core.api.Assertions.assertThat;

class AppTest {
    @Test
    void testMain() throws Exception {
        String result = tapSystemOut(() -> {
            App.main(null);
        });
        assertThat(result.trim()).isEqualTo("Hello! This is Hexlet!");
    }
}
