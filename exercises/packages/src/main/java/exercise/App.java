// BEGIN
package exercise;

import exercise.geometry.Point;
import exercise.geometry.Segment;

public class App {

    public static double[] getMidpointOfSegment(double[][] segment){
        double midPointX = (Point.getX(Segment.getBeginPoint(segment)) + Point.getX(Segment.getEndPoint(segment))) / 2;
        double midPointY = (Point.getY(Segment.getBeginPoint(segment)) + Point.getY(Segment.getEndPoint(segment))) / 2;
        double[] middlePointOfSegment = Point.makePoint(midPointX, midPointY);

        return middlePointOfSegment;
    }

    public static double[][] reverse(double[][] segment){
        double[][] reverseSegment = Segment.makeSegment(segment[1].clone(), segment[0].clone());

        return  reverseSegment;
    }

    public static int getQuadrant(double[] quadrant){

        if(Point.getX(quadrant) > 0 && Point.getY(quadrant) > 0){
            return 1;
        }else if(Point.getX(quadrant) < 0 && Point.getY(quadrant) > 0){
            return 2;
        }else if(Point.getX(quadrant) < 0 && Point.getY(quadrant) < 0){
            return 3;
        }else if(Point.getX(quadrant) > 0 && Point.getY(quadrant) < 0){
            return 4;
        }else{
            return 0;
        }
    }

    public static boolean isBelongToOneQuadrant(double[][] segment) {
        if (getQuadrant(Segment.getBeginPoint(segment)) == getQuadrant(Segment.getEndPoint(segment))) {
            return true;
        }
        return false;
    }
}
// END
