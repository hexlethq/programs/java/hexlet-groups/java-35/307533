package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int l1, int l2, int l3) {
        String type = "";
        if ((l1 + l2) < l3) {
            type = "Треугольник не существует";
        } else if (l1 != l2 && l1 != l3 && l2 != l3) {
            type = "Разносторонний";
        } else if (l1 == l2 && l1 == l3) {
            type = "Равносторонний";
        } else {
            type = "Равнобедренный";
        }
            return type;

    }

    //самостоятельная работа
    public static int getFinalGrade(int exam, int project){
        int finalGrade = 0;
        if (exam > 90 || project > 10){
            finalGrade = 100;
        }else if(exam > 75 && project >= 5){
            finalGrade = 90;
        }else if(exam > 50 && project >= 2){
            finalGrade = 75;
        }
        return finalGrade;
    }

    // END
}
