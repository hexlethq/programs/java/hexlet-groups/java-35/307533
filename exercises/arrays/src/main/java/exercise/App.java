package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] reverse(int[] num) {
        int[] num2 = new int[num.length];
        if (num.length == 0) {
            return num;
        }
        for (int i = 0; i < num.length; i++) {
            num2[i] = num[num.length - 1 - i];
        }
        return num2;
    }


    public static int mult(int[] num3) {
        int result = 1;
        for (int nume3 : num3) {
            result *= nume3;
        }
        return result;
    }

    //самостоятельная работа
    public static int[] flattenMatrix(int[][] matrix) {
        int[] emptyArr = new int[0];
        int counter = 0;
        int version = 0;
        if (matrix.length == 0) return emptyArr;
        int[] result = new int[matrix.length * matrix[0].length];

        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                version = i * +matrix[0].length + k;
                result[version] = matrix[i][k];
                counter++;
            }
        }

        return result;
    }

    // END
}
