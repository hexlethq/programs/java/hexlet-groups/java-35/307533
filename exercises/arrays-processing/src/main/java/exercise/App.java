package exercise;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] arr){
        int indexNegative = -1;
        int maxNegative = Integer.MIN_VALUE;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < 0 && arr[i] > maxNegative ) {
                maxNegative = arr[i];
                indexNegative = i;
            }
        }

        return indexNegative;
    }

    public static int[] getElementsLessAverage(int[] arr2){
        int sum = 0;
        if (arr2.length == 0){
            return new int[0];
        }
        for (int a: arr2){
            sum +=a;
        }
        int average = sum / arr2.length;
        int lenghtOfArray = 0;
        for (int b: arr2){
            if(b <= average){
                lenghtOfArray++;
            }
        }
        int[] result = new int[lenghtOfArray];
        int index = 0;
        for (int c: arr2){
            if(c <= average){
               result[index] = c;
               index++;
            }
        }
    return result;
    }

    //самостоятельная работа
    public static int getSumBeforeMinAndMax(int[] arr3){
        int min = Integer.MAX_VALUE;
        int indexMin = 0;
        int indexMax = 0;
        int sum2 = 0;
        for (int a : arr3) {
            min = a < min ? a : min;
        }
        int max = Integer.MIN_VALUE;
        for (int a : arr3) {
            max = a > max ? a : max;
        }
        for (int i = 0; i < arr3.length; i++) {
            if (arr3[i] == min) {
                indexMin = i;
            }
        }
        for (int i2 = 0; i2 < arr3.length; i2++) {
            if (arr3[i2] == max) {
                indexMax = i2;
            }
        }
        for(int i = indexMax; i < arr3.length; i--){
            if (i != indexMin){
                sum2 = arr3[indexMax] + arr3[indexMax-1];
            }
        }
        return sum2;
    }
    // END
}
