package exercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


// BEGIN
public class Kennel {
    public static String[][] puppysKennel = new String[0][0];
    public static int counter = 0;

    public static void addPuppy(String[] puppy) {
        puppysKennel = Arrays.copyOf(puppysKennel, counter + 1);
        puppysKennel[counter] = puppy;
        counter++;
    }

    public static void addSomePuppies(String[][] puppy) {
        for (String[] puppies : puppy) {
            addPuppy(puppies);
        }
    }

    public static int getPuppyCount() {
        return puppysKennel.length;
    }

    public static boolean isContainPuppy(String puppiesName) {
        for (int i = 0; i < puppysKennel.length; i++) {
            if (puppysKennel[i][0].equals(puppiesName)) return true;
        }
        return false;
    }

    public static String[][] getAllPuppies() {
        return puppysKennel;
    }

    public static String[] getNamesByBreed(String breed) {
        String[] namesByBreed = new String[0];
        int puppiesCounter = 0;
        for (String[] puppirsBreed : puppysKennel) {
            if (puppirsBreed[1].equals(breed)) {
                namesByBreed = Arrays.copyOf(namesByBreed, namesByBreed.length + 1);
                namesByBreed[puppiesCounter] = puppirsBreed[0];
                puppiesCounter++;
            }
        }
        return namesByBreed;
    }

    public static void resetKennel() {
        if (puppysKennel.length == 0) return;
        puppysKennel = Arrays.copyOf(puppysKennel, 0);
        counter = 0;
    }

    //самостоятельная работа
    public static boolean removePuppy(String puppiesName) {
        String[][] expected = new String[puppysKennel.length - 1][];
        for (int i = 0; i < puppysKennel.length; i++) {
            if (puppysKennel[i][0].equals(puppiesName)) {
                System.arraycopy(puppysKennel, 0, expected, 0, i);
                System.arraycopy(puppysKennel, i + 1, expected, i, puppysKennel.length - (i + 1));
                resetKennel();
                Kennel.addSomePuppies(expected);
                return true;
            }
        }
        return false;
}


}
// END
