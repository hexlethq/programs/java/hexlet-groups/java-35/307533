package exercise;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

class AppTest {

    @BeforeEach
    void clearStaticVariable() throws Exception {
        Kennel.resetKennel();
    }

    @Test
    void testKennel() {
        Kennel.addPuppy(new String[] {"Rex", "boxer"});
        assertThat(Kennel.getPuppyCount()).isEqualTo(1);

        String[][] puppies = {
            {"Rocky", "terrier"},
            {"Buddy", "chihuahua"},
            {"Toby", "chihuahua"},
        };

        Kennel.addSomePuppies(puppies);
        assertThat(Kennel.getPuppyCount()).isEqualTo(4);
        assertThat(Kennel.isContainPuppy("Buddy")).isTrue();
        assertThat(Kennel.isContainPuppy("Olson")).isFalse();

        String[][] actual1 = Kennel.getAllPuppies();
        String[][] expected1 = {
            {"Rex", "boxer"},
            {"Rocky", "terrier"},
            {"Buddy", "chihuahua"},
            {"Toby", "chihuahua"},
        };
        assertThat(actual1).isDeepEqualTo(expected1);

        String[] actual2 = Kennel.getNamesByBreed("chihuahua");
        assertThat(actual2).containsOnly("Buddy", "Toby");

        Kennel.resetKennel();
        assertThat(Kennel.getPuppyCount()).isEqualTo(0);
    }

    // BEGIN
    @Test
    void testRemove(){
        String[][] puppies= {
            {"Tedddy", "dog"},
            {"Lessie", "doberman"},
            {"Rex", "boxer"},
            {"Rocky", "terrier"},
            {"Buddy", "chihuahua"},
            {"Toby", "chihuahua"},
            {"Vegas", "Zimenhund"}
        };
        Kennel.addSomePuppies(puppies);
        Kennel.removePuppy("Rex");
        Kennel.removePuppy("Jack");
        Kennel.removePuppy("Lessie");

        String[][] allpuppies = Kennel.getAllPuppies();
        System.out.println(Arrays.deepToString(allpuppies));
    }
    // END
}
