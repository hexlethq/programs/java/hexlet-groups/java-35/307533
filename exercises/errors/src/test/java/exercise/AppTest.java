package exercise;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOut;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;

class AppTest {
    @Test
    void testPrintSquare() throws Exception {
        Circle circle = new Circle(new Point(2, 3), 5);
        String result = tapSystemOut(() -> {
            App.printSquare(circle);
        });
        assertThat(result.trim()).isEqualTo("79\nВычисление окончено");

        Circle circle1 = new Circle(new Point(2, 3), -5);
        Throwable thrownWithWrongRadius = catchThrowable(() -> App.printSquare(circle1));

        assertThat(thrownWithWrongRadius).isInstanceOf(NegativeRadiusException.class);
        assertThat(thrownWithWrongRadius.getMessage())
                .isEqualTo("Не удалось посчитать площадь\nВычисление окончено");
    }
}
