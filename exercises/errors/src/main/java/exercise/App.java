package exercise;

// BEGIN
public class App {
    public static void printSquare(Circle circle) throws NegativeRadiusException {
            try {
                circle.getSquare();
            } catch (NegativeRadiusException e) {
                System.out.println(e.getMessage());
            } finally {
                System.out.println((int) Math.ceil(circle.getSquare()) + "\nВычисление окончено");
            }
    }
}
// END
