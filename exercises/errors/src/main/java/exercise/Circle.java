package exercise;

// BEGIN
public class Circle {
    private Point point;
    private Integer radius;

    public Circle(Point point, Integer radius) {
        this.point = point;
        this.radius = radius;
    }

    public Integer getRadius() {
        return this.radius;
    }

    public double getSquare() throws NegativeRadiusException {
        if (getRadius() < 0) {
            throw new NegativeRadiusException("Не удалось посчитать площадь\nВычисление окончено");
        }
        return Math.PI * Math.pow(getRadius(), 2);
    }
}
// END
