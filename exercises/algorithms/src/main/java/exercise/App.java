package exercise;

class App {
    // BEGIN
    public static int[] sort(int[] arr){
       int number = 0;
       for( int i = arr.length -1; i >0; i--){
           for(int k = 0; k < i; k++){
               if(arr[k] > arr[k + 1]){
                   number = arr[k];
                   arr[k] = arr[k + 1];
                   arr[k + 1] = number;
               }
           }
       }
       return arr;
    }
    // END
}
