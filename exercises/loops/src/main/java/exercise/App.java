package exercise;


class App {
    // BEGIN
    public static void main(String[] args) {
        System.out.println(getAbbreviation("Enjoy Yourself"));
    }
    public static String getAbbreviation(String phrase){
        String ABR = String.valueOf(phrase.charAt(0));

        for (int i = 1; i < phrase.length(); i++) {
            if (phrase.charAt(i) != ' ' && phrase.charAt(i - 1) == ' ') {
                ABR += phrase.charAt(i);
            }
        }
        return (ABR.toUpperCase()).trim();


    }
    // END
}
