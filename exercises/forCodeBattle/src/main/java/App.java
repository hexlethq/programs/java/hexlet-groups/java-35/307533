import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class App {
    static final List<Integer> array = new ArrayList<>();
    static StringBuilder builder = new StringBuilder();
    static String unzipArray;
    static String serializedString;

    public static String serialize(List<Integer> array) throws IOException {
        generateArray();
        System.out.println("Строка до сжатия: " + array.toString());
        System.out.println("Размер строки до сжатия: " + array.toString().length());

        for (Integer i : array) {
            builder.append(i);
            builder.append(" ");
        }
        unzipArray = builder.toString().trim();

        if (unzipArray == null ||  unzipArray.length() <= 15) {
            return unzipArray;
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPOutputStream gzip = new GZIPOutputStream(out);
        gzip.write(unzipArray.getBytes());
        gzip.close();
        serializedString = out.toString(StandardCharsets.ISO_8859_1);
        System.out.println("Строка после сжатия: " + serializedString);
        System.out.println("Размер строки после сжатия: " + serializedString.length());

        return serializedString;
    }

    public static List<Integer> deserialize(String compressString) throws IOException {
        if (compressString == null || compressString.length() == 0) {
            return new ArrayList<>();
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayInputStream in = new ByteArrayInputStream(compressString.getBytes(StandardCharsets.ISO_8859_1));
        GZIPInputStream gunzip = new GZIPInputStream(in);
        byte[] buffer = new byte[256];
        int n;
        while ((n = gunzip.read(buffer)) >= 0) {
            out.write(buffer, 0, n);
        }

        List<Integer> uncompressed = new ArrayList<>();
        for (String s: out.toString().split(" ")) {
            uncompressed.add(Integer.valueOf(s));
        }
        return uncompressed;
    }

    public static void generateArray() {
        for (int i = 0; i < 15; i++) {
            array.add((int) (Math.random() * 1000));
        }
    }

    public static void main(String[] args) throws IOException{
        System.out.println(serialize(array));
        System.out.println(deserialize(serializedString));
    }

}