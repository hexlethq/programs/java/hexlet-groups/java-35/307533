package exercise;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Array {
    static final List<Integer> array = new ArrayList<>();
    static StringBuilder builder = new StringBuilder();
    static StringBuilder builder2 = new StringBuilder();
    static String unzipArray;
    static String compressString;

    public static void main(String[] args) {
        generateArray();

        System.out.println(unzipArray);
        System.out.println(unzipArray.length());

        System.out.println(compressString);
        System.out.println(compressString.length());
    }

    public static void generateArray() {
        for (int i = 0; i <  201; i++) {
            array.add((int) (Math.random() * 1001));
        }

        Collections.sort(array);

        for (Integer i : array) {
            builder.append(i);
            builder.append(" ");
        }

        for (Integer i : array) {
            if (i < 10) {
                builder2.append(i);
                builder2.append(" ");
            } else if (i % 2 == 0) {
                builder2.append("a");
                builder2.append(" ");
            } else if (i % 3 == 0) {
                builder2.append("b");
                builder2.append(" ");
            } else if (i % 4 == 0) {
                builder2.append("c");
                builder2.append(" ");
            } else if (i % 5 == 0) {
                builder2.append("d");
                builder2.append(" ");
            } else if (i % 6 == 0) {
                builder2.append("e");
                builder2.append(" ");
            } else if (i % 7 == 0) {
                builder2.append("f");
                builder2.append(" ");
            } else if (i % 8 == 0) {
                builder2.append("g");
                builder2.append(" ");
            } else if (i % 9 == 0) {
                builder2.append("h");
                builder2.append(" ");
            } else if (i % 10 == 0) {
                builder2.append("i");
                builder2.append(" ");
            } else if (i % 11 == 0) {
                builder2.append("j");
                builder2.append(" ");
            } else if (i % 12 == 0) {
                builder2.append("k");
                builder2.append(" ");
            } else if (i % 13 == 0) {
                builder2.append("l");
                builder2.append(" ");
            } else if (i % 14 == 0) {
                builder2.append("m");
                builder2.append(" ");
            } else if (i % 15 == 0) {
                builder2.append("n");
                builder2.append(" ");
            } else if (i % 16 == 0) {
                builder2.append("o");
                builder2.append(" ");
            } else if (i % 17 == 0) {
                builder2.append("p");
                builder2.append(" ");
            } else if (i % 18 == 0) {
                builder2.append("q");
                builder2.append(" ");
            } else if (i % 19 == 0) {
                builder2.append("r");
                builder2.append(" ");
            } else if (i % 20 == 0) {
                builder2.append("s");
                builder2.append(" ");
            } else if (i % 21 == 0) {
                builder2.append("t");
                builder2.append(" ");
            } else if (i % 22 == 0) {
                builder2.append("u");
                builder2.append(" ");
            } else if (i % 23 == 0) {
                builder2.append("v");
                builder2.append(" ");
            } else if (i % 24 == 0) {
                builder2.append("w");
                builder2.append(" ");
            } else if (i % 25 == 0) {
                builder2.append("x");
                builder2.append(" ");
            } else if (i % 26 == 0) {
                builder2.append("2");
                builder2.append(" ");
            } else if (i % 27 == 0) {
                builder2.append("z");
                builder2.append(" ");
            } else {
                builder2.append("i");
                builder2.append(" ");
            }
        }
        unzipArray = builder.toString().trim();
        compressString = builder2.toString().trim();
    }

//    public static String abc(Integer i) {
//        switch (i):
//    }
}
